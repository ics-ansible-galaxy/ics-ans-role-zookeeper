# ics-ans-role-zookeeper

Ansible role to install Zookeeper.

## Role Variables

```yaml
zookeeper_network: zookeeper-network
zookeeper_container_name: zookeeper
zookeeper_image: zookeeper
zookeeper_client_port: 2181
zookeeper_peer_port: 2888
zookeeper_leader_port: 3888
zookeeper_data: zookeeper-data
zookeeper_datalog: zookeeper-datalog
zookeeper_env: {}
zookeeper_replication_id: "1"
zookeeper_replication_servers: []
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-zookeeper
```

## License

BSD 2-clause
